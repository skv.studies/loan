package demo.loanapplication.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "loan")
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class Loan {
    @Id
    String id = UUID.randomUUID().toString();

    @NonNull
    Long sum;

    @NonNull
    String goal;

    @NonNull
    String loanType;

    @NonNull
    String documents;

    @NonNull
    Boolean approved;

    @NonNull
    Integer rate;
}
