package demo.loanapplication.execution;

import demo.loanapplication.service.LoanServiceImpl;
import demo.loanapplication.utils.ProcessVariable;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
public class LoanDelegate implements JavaDelegate {
    private final LoanServiceImpl loanService;

    public LoanDelegate(LoanServiceImpl loanService) {
        this.loanService = loanService;
    }

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        Long sum = (Long) delegateExecution.getVariable(ProcessVariable.SUM);
        String goal = (String) delegateExecution.getVariable(ProcessVariable.GOAL);
        String loanType = (String) delegateExecution.getVariable(ProcessVariable.LOANTYPE);
        String documents = (String) delegateExecution.getVariable(ProcessVariable.DOCUMENTS);
        Boolean approved = (Boolean) delegateExecution.getVariable(ProcessVariable.APPROVED);
        Integer rate = (Integer) delegateExecution.getVariable(ProcessVariable.RATE);
        loanService.create(sum, goal, loanType, documents, approved, rate);
    }
}
