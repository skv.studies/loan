package demo.loanapplication.utils;

public interface ProcessVariable {
    String SUM = "sum";
    String GOAL = "goal";
    String LOANTYPE = "loanType";
    String DOCUMENTS = "document";
    String APPROVED = "approved";
    String RATE = "rate";
    String VALID = "valid";
}