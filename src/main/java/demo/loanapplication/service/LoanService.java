package demo.loanapplication.service;

public interface LoanService {
    void create(Long sum, String goal, String loanType, String documents, Boolean approved, Integer rate);
}
