package demo.loanapplication.service;

import demo.loanapplication.entity.Loan;
import demo.loanapplication.repository.LoanRepository;
import org.springframework.stereotype.Service;

@Service
public class LoanServiceImpl implements LoanService {
    private final LoanRepository loanRepository;

    public LoanServiceImpl(LoanRepository loanRepository) {
        this.loanRepository = loanRepository;
    }

    @Override
    public void create(Long sum, String goal, String loanType, String documents, Boolean approved, Integer rate) {
        loanRepository.saveAndFlush(new Loan(sum, goal, loanType, documents, approved, rate));
    }
}
